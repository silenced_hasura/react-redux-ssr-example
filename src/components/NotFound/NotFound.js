import React from 'react';

const NotFound = () => {
  return (
    /* Use global styles normally */
    <div className="container-fluid">
      {/* Use CSS Modules to use the local styles */}
      <h1>401 iruku saar</h1>
      <div>The path you request is not found.</div>
    </div>
  );
};

export default NotFound;
